#include "PlainText.h"
#include "ShiftText.h"
#include "CaesarText.h"
#include "SubstitutionText.h"
#include "FileHelper.h"

#include <iostream>
#include "string.h"

// MEMORY LEAK CHECK
#define _CRTDBG_MAP_ALLOC 
#include <crtdbg.h>


void checkPlain();
void checkShift();
void checkCaesar();
void checkSubstitution();
void checkFileHelper();
void checkOstreamOverloading();


int main()
{
	//checkPlain();

	//checkShift();

	//checkCaesar();

	//checkSubstitution();

	//checkFileHelper();

	checkOstreamOverloading();

	printf("Leaks: %d\n", _CrtDumpMemoryLeaks());  //Checks memory leak

	return 0;
}


void checkPlain()
{
	PlainText pt("TEST");

	std::cout << pt.getText() << std::endl;
	std::cout << pt.isEncrypted() << std::endl;
}


void checkShift()
{
	ShiftText st("an apple a day keeps anyone away if you throw it hard enough.", 7);

	std::cout << st.encrypt() << std::endl;  //checks if it actually encrypts
	std::cout << st.getText() << std::endl;  //checks if the encrypted text is saved in PlainText
	std::cout << st.isEncrypted() << std::endl;  //checks if isEnc element is correct

	std::cout << st.decrypt() << std::endl;  //checks if it actually decrypts
	std::cout << st.getText() << std::endl;  //checks if the decrypted text is saved in PlainText
	std::cout << st.isEncrypted() << std::endl;  //checks if isEnc element is correct
}


void checkCaesar()
{
	CaesarText ct("roses are red, my name is dave, this makes no sense, microwave.");

	std::cout << ct.encrypt() << std::endl;  //checks if it actually encrypts
	std::cout << ct.getText() << std::endl;  //checks if the encrypted text is saved in PlainText
	std::cout << ct.isEncrypted() << std::endl;  //checks if isEnc element is correct

	std::cout << ct.decrypt() << std::endl;  //checks if it actually decrypts
	std::cout << ct.getText() << std::endl;  //checks if the decrypted text is saved in PlainText
	std::cout << ct.isEncrypted() << std::endl;  //checks if isEnc element is correct
}


void checkSubstitution()
{
	SubstitutionText subt("sometimes when i close my eyes, i cant see.", "dictionary.csv");

	std::cout << subt.encrypt() << std::endl;  //checks if it actually encrypts
	std::cout << subt.getText() << std::endl;  //checks if the encrypted text is saved in PlainText
	std::cout << subt.isEncrypted() << std::endl;  //checks if isEnc element is correct

	std::cout << subt.decrypt() << std::endl;  //checks if it actually decrypts
	std::cout << subt.getText() << std::endl;  //checks if the decrypted text is saved in PlainText
	std::cout << subt.isEncrypted() << std::endl;  //checks if isEnc element is correct
}


void checkFileHelper()
{
	std::cout << FileHelper::readFileTostring("dictionary.csv") << std::endl;
	FileHelper::writeWordsToFile("dictionary.csv", "paste.txt");
}


void checkOstreamOverloading()
{
	PlainText plaint("abc");
	ShiftText shiftt("abc", 5);
	CaesarText caesart("abc");
	SubstitutionText subt("abc", "dictionary.csv");

	std::cout << plaint << std::endl;
	std::cout << shiftt << std::endl;
	std::cout << caesart << std::endl;
	std::cout << subt << std::endl;
}

