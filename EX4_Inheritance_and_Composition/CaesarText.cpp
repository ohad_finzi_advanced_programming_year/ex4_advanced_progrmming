#include "CaesarText.h"


/*
The function constructs the current CaesarText class according to the given elements
Input: string which contains the text itself 
*/
CaesarText::CaesarText(std::string text) :
	ShiftText(text, DEFAULT_KEY)
{
	//no need to init anything...
}


/*
The fucntiosn diconstructs the current CaesarText class
*/
CaesarText::~CaesarText()
{
	//no need to do anything... no dynamic memory allocated.
}


/*
The function uses the encrypt function from ShiftText class
Ouput: the encrypted _text
*/
std::string CaesarText::encrypt()
{
	return ShiftText::encrypt();
}


/*
The function uses the decrypt function from ShiftText class
Ouput: the decrypted _text
*/
std::string CaesarText::decrypt()
{
	return ShiftText::decrypt();
}


/*
The function overloads the std::cout<< operator and instead prints the current class text encrypted
Input: the CaesarText class to print its text encripted and the ostream that uses the std::cout - print funciton
Output: the CaesarText class text encrypted
*/
std::ostream& operator<<(std::ostream& os, CaesarText& curr)
{
	std::cout << curr.encrypt();
	return os;
}

