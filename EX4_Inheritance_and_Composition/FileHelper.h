#ifndef FILEHELPER_H
#define FILEHELPER_H

#include <iostream>
#include <string>
#include <fstream>


class FileHelper
{
public:
	//methods
	static std::string readFileTostring(const std::string fileName);
	static void writeWordsToFile(const std::string inputFileName, const std::string outputFileName);
	static void writeStringTofile(const std::string text, const std::string outputFileName);
};


#endif

