#ifndef PLAINTEXT_H
#define PLAINTEXT_H

#include <iostream>
#include <string>

#define LETTERS_NUM 26
#define FIRST_LETTER 97  //the letter before a in ascii code
#define LAST_LETTER 122  //the letter z in ascii code


class PlainText
{
private:
	static int _numOfTexts;

protected:
	//fields
	std::string _text;
	bool _isEncrypted;

public:
	//methods
	PlainText(std::string text);
	~PlainText();
	bool isEncrypted() const;
	std::string getText() const;

	friend std::ostream& operator<<(std::ostream& os, PlainText& curr);

	static int getNumOfTexts(); 
};


#endif

