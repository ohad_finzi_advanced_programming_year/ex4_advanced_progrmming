#include "PlainText.h"
#include "ShiftText.h"
#include "CaesarText.h"
#include "SubstitutionText.h"
#include "FileHelper.h"

#include <iostream>
#include <string>

#define SUBSTITUTIONTEXT "substitution"
#define CAESARTEXT "caesar"
#define SHIFTTEXT "shift"

//for the memory leak tests
//#define _CRTDBG_MAP_ALLOC 
//#include <crtdbg.h> 

int menu();
std::string getCipher(std::string text, int option);
bool checkFilePath(std::string filePath);
void option1();
void option2();


int main()
{
	int mainChoice = 0;

	while (mainChoice != 4)  //runs until the user will enter the option 4 (exit)
	{
		mainChoice = menu();

		switch (mainChoice)
		{
		case 1:
			option1();
			break;

		case 2:
			option2();
			break;

		case 3:
			std::cout << "\nThe number of times you used one of our decoders are: " << PlainText::getNumOfTexts() << std::endl;
			break;

		case 4:
			std::cout << "BYE BYE! Make sure to like, subscribe and turn on nofitication to never miss another video!!!" << std::endl;
			break;

		default:
			std::cout << "Invalid option! Try again" << std::endl;
			break;
		}
	}

	//printf("Leaks: %d", _CrtDumpMemoryLeaks());  //checks if there are any memory leaks

	return 0;
}


/*
The function print the menu for the user. it shows all the capabilities of this project.
Output: the user's option of which function to use of the project
*/
int menu()
{
	int choice = 0;

	std::cout << "\nHello and welcome to our profesional decryption/encryption service!" << std::endl;
	std::cout << "In our service you can do multiple things using our Shift, Caeser and Substitution ciphers!" << std::endl;
	std::cout << "Choose one of the options below:" << std::endl;
	std::cout << "1 - decrypts any text using one of our built in decoders" << std::endl;
	std::cout << "2 - decrypts/encrypts any file using one of our built in decoders" << std::endl;
	std::cout << "3 - print number of time you used one of our decoders" << std::endl;
	std::cout << "4 - exit" << std::endl;
	std::cout << "Enter your choice here: ";

	std::cin >> choice;
	getchar();  //cleans the buffer

	return choice;
}


/*
The function usues the classes encrypt and decrypt according to the user requests
Input: the text to decrypt/encrypt, the option between encrypt (1) and decrypt (0)
Output: the encrypted/decrypted string
*/
std::string getCipher(std::string text, int option = 0)
{
	std::string cipher = "";
	std::string dictionary = "";
	std::string ans = "";
	int key = 0;
	bool flag = true;

	while (flag)  //a loop that runs until the user managed to encrypt/decrypt his wanted text
	{
		flag = false;  //resets the flag

		std::cout << "Enter which cipher you would like to use ('substitution', 'shift' and 'caesar'): ";
		std::getline(std::cin, cipher);

		if (cipher == SUBSTITUTIONTEXT)
		{
			std::cout << "Enter a .csv file path to use as a dictionary for the cipher: ";
			std::getline(std::cin, dictionary);

			while (!(dictionary.find(".csv") != std::string::npos) || !checkFilePath(dictionary))  //runs until the user enters a valid .csv file path
			{
				std::cout << "Please enter a valid .csv file path: ";
				std::getline(std::cin, dictionary);
			}

			SubstitutionText subt(text, dictionary);
			if (option)  //if the user wantes to the encrypt
			{
				ans = subt.encrypt();
			}
			else  //if the user wantes to the decrypt
			{
				ans = subt.decrypt();
			}
		}
		else if (cipher == SHIFTTEXT)
		{
			std::cout << "Enter a key to use in the Shift Key cipher: ";
			std::cin >> key;
			getchar();  //cleans the buffer

			while (key < 0)
			{
				std::cout << "Please enter a positive number: ";
				std::cin >> key;
				getchar();  //cleans the buffer
			}

			ShiftText shiftt(text, key);
			if (option)  //if the user wantes to the encrypt
			{
				ans = shiftt.encrypt();
			}
			else  //if the user wantes to the decrypt
			{
				ans = shiftt.decrypt();
			}
		}
		else if (cipher == CAESARTEXT)
		{
			CaesarText caesart(text);
			if (option)  //if the user wantes to the encrypt
			{
				ans = caesart.encrypt();
			}
			else  //if the user wantes to the decrypt
			{
				ans = caesart.decrypt();
			}
		}
		else  //incase the user entered a non existed cipher code
		{
			std::cout << "Invalid option! Try again" << std::endl;
			flag = true;  //keeps the loop running until it gets an exited cipher code
		}
	}
	
	return ans;
}


/*
The function checks if the given file path is real and usable
Input: the file path to check
Output: true if the file path is usable and false if not
*/
bool checkFilePath(std::string filePath)
{
	std::fstream openFile(filePath);
	bool ans = false;

	if (openFile.is_open())
	{
		ans = true;
		openFile.close();
	}

	return ans;
}


/*
The functions does the option1 according to the exercise: gets an encrypted text from the user, decrypts using the wanted cipher code and prints th result
*/
void option1()
{
	std::string text = "";
	std::string decoded = "";

	std::cout << "Enter a string you would like to decrypt: ";
	std::getline(std::cin, text);

	decoded = getCipher(text);
	std::cout << "The decoded text: \n" << decoded << std::endl;
}


/*
The function does the option2 according to the exercise: gets an input file from the user and encrypts/decrypts it (according to his wish)
and prints the result or puts it in another file (according to his wish)
*/
void option2()
{
	std::string inputString = "";
	std::string inputFile = "";
	std::string outputFile = "";
	std::string decoded = "";
	int cipherOption = 0;
	int printOption = 0;

	std::cout << "Enter an input .txt file you would like to cipher: ";
	std::getline(std::cin, inputFile);

	while (!(inputFile.find(".txt") != std::string::npos) || !checkFilePath(inputFile))  //runs until the user enters a valid .txt file path
	{
		std::cout << "Please enter a .txt valid file path: ";
		std::getline(std::cin, inputFile);
	}

	do
	{
		std::cout << "Enter 0 if you would like to decrypt the file, and 1 if you would like to encrypt it: ";
		std::cin >> cipherOption;
		getchar();  //cleans the buffer
	}
	while (cipherOption != 1 && cipherOption != 0);  //runs until the user enters 1 (encrypt) or 0 (decrypt)

	do
	{
		std::cout << "Enter 0 if you would like print the result, and 1 if you would like to create a new file with the result: ";
		std::cin >> printOption;
		getchar();  //cleans the buffer
	}
	while (printOption != 1 && printOption != 0);  //runs until the user enters 1 (save result in another file) or 0 (print the result)

	if (printOption)  //if the user wants to save the result in another file
	{
		std::cout << "Enter an output .txt file you would like to put the cipher in it: ";
		std::getline(std::cin, outputFile);

		while (!(outputFile.find(".txt") != std::string::npos))  //runs until the user enters a .txt file path
		{
			std::cout << "Please enter a .txt file path: ";
			std::getline(std::cin, outputFile);
		}

		FileHelper::writeWordsToFile(inputFile, outputFile);  //writes the input file to the output file
		inputString = FileHelper::readFileTostring(outputFile);  //reads the output file into a string
		decoded = getCipher(inputString, cipherOption);  //decrypts/encrypts the string (according to user's wish)
 		FileHelper::writeStringTofile(decoded, outputFile);  //writes the decrypts/encrypts string to the output file

		std::cout << "The file " << outputFile << " now holds the encrypted/decrypted string that is in the file " << inputFile << std::endl;
	}
	else  //if the user wants to print the result
	{
		inputString = FileHelper::readFileTostring(inputFile);
		decoded = getCipher(inputString, cipherOption);

		std::cout << "The encrypted/decrypted string from the input file:\n"<< decoded << std::endl;
	}
}

