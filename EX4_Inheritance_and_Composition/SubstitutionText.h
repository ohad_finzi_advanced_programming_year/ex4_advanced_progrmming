#ifndef SUBSTITUTIONTEXT_H
#define SUBSTITUTIONTEXT_H

#include <fstream>
#include "PlainText.h"


class SubstitutionText : public PlainText
{
private:
	//fields
	std::string _dictionaryFileName;

public:
	//methods
	SubstitutionText(const std::string text, const std::string dictionaryFileName);
	~SubstitutionText();
	std::string encrypt();
	std::string decrypt();

	friend std::ostream& operator<<(std::ostream& os, SubstitutionText& curr);
};


#endif

