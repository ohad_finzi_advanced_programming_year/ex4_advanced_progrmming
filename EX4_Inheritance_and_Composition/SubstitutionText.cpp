#include "SubstitutionText.h"


/*
The function constructs the current SubstitutionText class according to the given elements
Input: string which contains the text itself and the dictionary file which contains the cipher
*/
SubstitutionText::SubstitutionText(std::string text, std::string dictionaryFileName) :
	PlainText(text)
{
	this->_dictionaryFileName = dictionaryFileName;
}


/*
The fucntiosn diconstructs the current SubstitutionText class
*/
SubstitutionText::~SubstitutionText()
{
	//no need to do anything... no dynamic memory allocated.
}


/*
The function encrypts _text according to the given dictionary file
Output: the encrypted _text
*/
std::string SubstitutionText::encrypt()
{
	std::ifstream myfile(_dictionaryFileName);
	std::string currLine = "";
	std::string encryptedString = "";
	char currLetter = 'a';
	int i = 0;

	if (myfile.is_open())  //checks if the file is valid
	{
		for (i = 0; i < _text.length(); i++)  //runs on the string to encrypt
		{
			currLine = "";  //resets the line

			currLetter = _text[i];
			while (std::getline(myfile, currLine))  //a loop that runs on the given file and extracts each line
			{
				if (currLine[0] == currLetter)  //checks if the decrypted letter (the letter in the index 2 in each line of the file is decrypted) is equal to the current letter in _text
				{
					currLetter = currLine[2];  //takes the encrypted letter (this letter is in index 2 in each line of the file) of the decrypted letter (this letter is in index 0 in each line of the file)
					break;
				}
			}

			myfile.clear();  //resets the file flags  
			myfile.seekg(0);  //reset the file to the beggining

			encryptedString += currLetter;
		}

		myfile.close();
	}
	else  //incase the file isnt valid
	{ 
		std::cerr << "Unable to open file" << std::endl;
		exit(-1);
	}

	_text = encryptedString;
	_isEncrypted = true;

	return _text;
}


/*
The function decrypts _text according to the given dictionary file
Output: the decrypted _text
*/
std::string SubstitutionText::decrypt()
{
	std::ifstream myfile(_dictionaryFileName);
	std::string currLine = "";
	std::string encryptedString = "";
	char currLetter = 'a';
	int i = 0;

	if (myfile.is_open())  //checks if the file is valid
	{
		for (i = 0; i < _text.length(); i++)
		{
			currLine = "";  //resets the line

			currLetter = _text[i];
			while (std::getline(myfile, currLine))  //a loop that runs on the given file and extracts each line
			{
				if (currLine[2] == currLetter)  //checks if the encrypted letter (the letter in the index 2 in each line of the file is encrypted) is equal to the current letter in _text
				{
					currLetter = currLine[0];  //takes the decrypted letter (this letter is in index 0 in each line of the file) of the encrypted letter (this letter is in index 2 in each line of the file)
					break;
				}
			}

			myfile.clear();  //resets the file flags  
			myfile.seekg(0);  //reset the file to the beggining

			encryptedString += currLetter;
		}

		myfile.close();
	}
	else  //incase the file isnt valid
	{
		std::cerr << "Unable to open file" << std::endl;
		exit(-1);
	}

	_text = encryptedString;
	_isEncrypted = false;

	return _text;
}


/*
The function overloads the std::cout<< operator and instead prints the current class text encrypted
Input: the SubstitutionText class to print its text encripted and the ostream that uses the std::cout - print funciton
Output: the SubstitutionText class text encrypted
*/
std::ostream& operator<<(std::ostream& os, SubstitutionText& curr)
{
	std::cout << curr.encrypt();
	return os;
}

