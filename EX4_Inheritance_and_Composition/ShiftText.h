#ifndef SHIFTTEXT_H
#define SHIFTTEXT_H

#include <iostream>
#include "PlainText.h"


class ShiftText : public PlainText
{
private:
	//fields
	int _key;

public:
	//methods
	ShiftText(std::string text, int key);
	~ShiftText();
	std::string encrypt();
	std::string decrypt();

	friend std::ostream& operator<<(std::ostream& os, ShiftText& curr);
};


#endif

