#ifndef CAESARTEXT_H
#define CAESARTEXT_H

#include <iostream>
#include "ShiftText.h"

#define DEFAULT_KEY 3  //the default key that being used in Caesar cipher


class CaesarText : public ShiftText
{
public:
	//methods
	CaesarText(std::string text);
	~CaesarText();
	std::string encrypt();
	std::string decrypt();

	friend std::ostream& operator<<(std::ostream& os, CaesarText& curr);
};


#endif

