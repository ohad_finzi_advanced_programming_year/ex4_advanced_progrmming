#include "ShiftText.h"


/*
The function constructs the current ShiftText class according to the given elements
Input: string which contains the text itself and key which contains the shift for the encryption
*/
ShiftText::ShiftText(std::string text, int key):
	PlainText(text)
{
	this->_key = key % LETTERS_NUM;  //to prevent a very big keys value
}


/*
The fucntiosn diconstructs the current ShiftText class
*/
ShiftText::~ShiftText()
{
	//no need to do anything... no dynamic memory allocated.
}


/*
The function encrypts _text according to _key and saves the result in _text which is in the PlainText class
Output: the encrypted _text
*/
std::string ShiftText::encrypt()
{
	std::string encryptedString = "";
	char currLetter = ' ';
	int i = 0;

	for (i = 0; i < _text.length(); i++)  //runs on the _text chars
	{
		currLetter = _text[i];  //gets the current char

		if (currLetter >= FIRST_LETTER && currLetter <= LAST_LETTER)  //checks if the current char is between 96 and 122 (between a and z)
		{
			currLetter -= FIRST_LETTER;  //converts the current letter of the text from ascii code to regular number. example: 97('a') -> 1
			currLetter = (currLetter + this->_key) % LETTERS_NUM;  //converts the current letter to encrypted letter
			currLetter += FIRST_LETTER;  //converts the current letter back to its ascii code. example: 1('a') -> 97
		}

		encryptedString += currLetter;
	}

	_text = encryptedString; 
	_isEncrypted = true; 

	return _text;
}


/*
The function decryptss _text according to _key and saves the result in _text which is in the PlainText class
Output: the decrypted _text
*/
std::string ShiftText::decrypt()
{
	std::string encryptedString = "";
	char currLetter = ' ';
	int i = 0;

	for (i = 0; i < _text.length(); i++)  //runs on the _text chars
	{
		currLetter = _text[i];  //get the current char

		if (currLetter >= FIRST_LETTER && currLetter <= LAST_LETTER)  //checks if the current char is between 96 and 122 (between a and z)
		{
			currLetter -= FIRST_LETTER;  //converts the current letter of the text from ascii code to regular number. example: 97('a') -> 1
			currLetter += LETTERS_NUM;  //incase if the key substraact the current letter below 0. example: 4('d') - 4(key) = 0 -> 'z'
			currLetter = (currLetter - this->_key) % LETTERS_NUM;  //converts the current letter to a decrypted letter
			currLetter += FIRST_LETTER;  //converts the current letter back to its ascii code. example: 1('a') -> 97
		}

		encryptedString += currLetter;
	}

	_text = encryptedString;
	_isEncrypted = false;

	return _text;
}


/*
The function overloads the std::cout<< operator and instead prints the current class text encrypted
Input: the ShiftText class to print its text encripted and the ostream that uses the std::cout - print funciton
Output: the ShiftText class text encrypted
*/
std::ostream& operator<<(std::ostream& os, ShiftText& curr)
{
	std::cout << curr.encrypt();
	return os;
}

