#include "PlainText.h"

int PlainText::_numOfTexts = 0;


/*
The function constructs the current PlainText class according to the given elements
Input: string which contains the text itself
*/
PlainText::PlainText(const std::string newText)
{
	this->_text = newText;
	this->_isEncrypted = false;  //default case

	_numOfTexts++;
}


/*
The fucntiosn diconstructs the current PlainText class
*/
PlainText::~PlainText()
{
	//no need to do anything... no dynamic memory allocated.
}


/*
The function returns the _isEncrypted element of the current class
OutputL isEncrypted
*/
bool PlainText::isEncrypted() const
{
	return this->_isEncrypted;
}


/*
The function returns the _text element of the current class
Output: text
*/
std::string PlainText::getText() const
{
	return this->_text;
}


/*
The function overloads the std::cout<< operator and instead prints the current class text encrypted
Input: the PlainText class to print its text encripted and the ostream that uses the std::cout - print funciton
Output: the PlainText class text encrypted
*/
std::ostream& operator<<(std::ostream& os, PlainText& curr)
{
	os << curr.getText();
	return os;
}


/*
The function gets the static element of the class: _numOFTexts
Output: the static element _numOfTexts 
*/
int PlainText::getNumOfTexts()
{
	return _numOfTexts;
}

