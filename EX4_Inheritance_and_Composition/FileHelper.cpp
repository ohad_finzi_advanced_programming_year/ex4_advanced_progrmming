#include "FileHelper.h"


/*
The function reads all the given file into a string and returns it
Input: the file name 
Output: the string which contains all the file 
*/
std::string FileHelper::readFileTostring(const std::string fileName)
{
	std::ifstream copyFile(fileName);
	std::string currLine = "";
	std::string allFile = "";

	if (copyFile.is_open())  //checks if the file name is valid
	{
		while (std::getline(copyFile, currLine))  //a loop that runs on the given file and extracts each line
		{
			allFile += currLine + "\n";  //puts each line inside the string
		}

		copyFile.close();
	}
	else  //incase the file isnt valid
	{
		std::cerr << "Unable to open file" << std::endl;
		exit(-1);
	}

	return allFile;
}


/*
The functions copies the first file name and pastes it in the second file name
Input: the file name to copy and the file name to paste it into
*/
void FileHelper::writeWordsToFile(const std::string inputFileName, const std::string outputFileName)
{
	std::ifstream copy(inputFileName);
	std::ofstream paste(outputFileName);
	std::string currWord = "";
	char checkEndLine = 'a';

	if (copy.is_open() && paste.is_open())  //checks if both files are valid
	{
		while (copy >> currWord)  //runs through all the words of the copy file
		{
			paste << currWord + ' ';  //pastes each copy file word (plus space between words) into paste file 

			copy.get(checkEndLine);  //gets the current character
			if (checkEndLine == '\n')  //checks if the current character is end of line
			{
				paste << '\n';  //adds the next line to the paste file
			}
		}

		copy.close();
		paste.close();
	}
	else  //incase one of the files is invalid
	{
		std::cerr << "Unable to open one of the files" << std::endl;
		exit(-1);
	}
}


/*
The function copies the given string into the given file path
Input: a string to copy to the given file path
*/
void FileHelper::writeStringTofile(const std::string text, const std::string outputFileName)
{
	std::ofstream paste(outputFileName);

	if (paste.is_open())  //checks if the file name is valid
	{
		paste << text;

		paste.close();
	}
	else  //incase the file isnt valid
	{
		std::cerr << "Unable to open file" << std::endl;
		exit(-1);
	}
}

